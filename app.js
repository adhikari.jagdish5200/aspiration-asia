const express = require("express");
const Cors = require("cors");
var bodyParser = require('body-parser');
var winston = require('./config/winston');
var morgan = require('morgan');
const userRoutes = require('./api/routes/userRoute');
const carouselRoutes = require('./api/routes/carouselRoute');
const serviceRoutes = require('./api/routes/serviceRoute');
const destinationRoutes = require('./api/routes/destinationRoute');

const mongoose = require('mongoose');
var app = express();
app.use(Cors());
app.use(morgan('combined', { stream: winston.stream }));


mongoose.connect('mongodb://localhost:27017/aspirationasia', {
  useUnifiedTopology: true,
  useNewUrlParser: true
})
  .then(() => console.log('Now connected to MongoDB!'))
  .catch(err => console.log('Something went wrong', err));

app.use(express.json());
app.use(express.urlencoded({ extended: false }));
app.use(bodyParser.urlencoded({ extended: false }));
app.use(bodyParser.json());
app.use(express.static('public'));

//Routes
app.use('/api', userRoutes);
app.use('/api', carouselRoutes);
app.use('/api', serviceRoutes);
app.use('/api', destinationRoutes);

app.use((req, res, next) => {
  const err = new Error('Route Not Found');
  console.log(err.stack);
  winston.error(`${err.status || 404} - ${err.message} - ${req.originalUrl} - ${req.method} - ${req.ip}`);
  err.status = 404;
  next(err);
});

app.use((err, req, res, next) => {
  res.status(err.status || 500);
  console.log(err.stack);
  winston.error(`${err.status || 500} - ${err.message} - ${req.originalUrl} - ${req.method} - ${req.ip}`);
  res.json({
    err: {
      message: err.message
    }
  })
});

module.exports = app;