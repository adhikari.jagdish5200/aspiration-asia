const mongoose = require('mongoose');
const Destination = require('../model/destinations');
var db = mongoose.connection;
var Schema = mongoose.Schema;
var destinationDetails = new Schema({
    destinationId: { type: Number, required: true }
});
var destinationObject = db.model('destinations', destinationDetails);
var destinationSort = { destinationId: -1 };
db.on('error', console.error);

//Add new Destination
exports.newDestination = (req, res, next) => {
    Destination.find({ destinationName: req.body.destinationName })
        .exec()
        .then(destination => {
            if (destination.length >= 1) {
                return res.status(200).json({
                    statusCode: false,
                    message: 'Destination already exists!'
                });
            } else {
                destinationObject.find({}, function (err, result) {
                    let dId;
                    if (result.length === 0) {
                        dId = 1
                    } else {
                        dId = result[0].destinationId + 1
                    }
                    if (err) {
                        res.status(500).json({
                            statusCode: false,
                            message: 'Something Went Wrong!',
                        });
                    } else {
                        const destination = new Destination(
                            {
                                destinationId: dId,
                                destinationName: req.body.destinationName,
                                imageUrl: req.body.imageUrl,
                                createdDate: new Date()
                            }
                        );
                        destination.save()
                            .then(result => {
                                return res.status(200).json({
                                    statusCode: true,
                                    message: 'Destination added successfully!',
                                });
                            })
                            .catch(err => {
                                res.status(500).json({
                                    statusCode: false,
                                    message: 'Error adding destination!',
                                });
                            });
                    }
                }).sort(destinationSort).limit(1);
            }
        })
        .catch(err => {
            console.log(err);
            res.status(500).json({
                error: err,
                statusCode: false,
                message: 'Something Went Wrong!',
            });
        });
}

//List all destinations / Search Destination by Id
exports.searchDestination = (req, res, next) => {
    let cond = {}

    if (!req.body.destinationId == "") {
        cond = { destinationId: req.body.destinationId };
    }

    destinationObject.find(cond
        , function (err, result) {
            if (result != null && result.length > 0) {
                return res.status(200).json({
                    statusCode: true,
                    message: 'Destinations Retrived Successfully!',
                    data: result
                });
            } else {
                return res.status(200).json({
                    statusCode: false,
                    message: 'No Destination Found!'
                });
            }

        });
}