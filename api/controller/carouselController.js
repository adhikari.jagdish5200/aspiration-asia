const Carousel = require('../model/carousel');
const mongoose = require('mongoose');
var db = mongoose.connection;
var Schema = mongoose.Schema;
var carouselDetails = new Schema({});
var carouselObject = db.model('carousels', carouselDetails);

//Add new Dashboard Carousel Image
exports.newCarousel = (req, res, next) => {
    Carousel.find({ imageUrl: req.body.imageUrl })
        .exec()
        .then(carousel => {
            if (carousel.length >= 1) {
                return res.status(200).json({
                    statusCode: false,
                    message: 'Carousel image already exists!'
                });
            } else {
                const carousel = new Carousel(
                    {
                        imageUrl: req.body.imageUrl,
                        createdDate: new Date()
                    }
                );
                carousel.save()
                    .then(result => {
                        return res.status(200).json({
                            statusCode: true,
                            message: 'Carousel added successfully!',
                        });
                    })
                    .catch(err => {
                        res.status(500).json({
                            statusCode: false,
                            message: 'Error adding Carousel!',
                        });
                    });
            }
        })
        .catch(err => {
            res.status(500).json({
                error: err,
                statusCode: false,
                message: 'Something Went Wrong! Please Try Again',
            });
        });
}

//List all carousels
exports.listCarousel = (req, res, next) => {

    carouselObject.find({}
        , function (err, result) {
            if (result != null && result.length > 0) {
                return res.status(200).json({
                    statusCode: true,
                    message: 'Carousels Retrived Successfully!',
                    data: result
                });
            } else {
                return res.status(200).json({
                    statusCode: false,
                    message: 'No Carousel Found!'
                });
            }

        }).catch(err => {
            res.status(500).json({
                error: err,
                statusCode: false,
                message: 'Something Went Wrong!',
            });
        });
}