const User = require('../model/user');
const bcrypt = require('bcrypt');
const jsonWebToken = require('jsonwebtoken');
const mongoose = require('mongoose');
var db = mongoose.connection;
var Schema = mongoose.Schema;
var userDetails = new Schema({
    userId: { type: Number, required: true }
});
var userObject = db.model('users', userDetails);
db.on('error', console.error);

exports.login = (req, res, next) => {

    User.find({ username: req.body.username })
        .exec()
        .then(user => {
            if (user.length < 1) {
                return res.status(200).json({
                    statusCode: false,
                    message: 'User Doesnot Exist!'
                });
            }
            bcrypt.compare(req.body.password, user[0].password, (err, result) => {

                if (result) {
                    const token = jsonWebToken.sign({
                        username: user[0].username,
                        userId: user[0]._id
                    }, "secret", {
                        expiresIn: "1h"
                    });
                    userObject.find({ username: req.body.username }, 'name userId username email',
                        function (err, result) {

                            if (err) {
                                return res.status(200).json({
                                    statusCode: false,
                                    message: 'Error fetching user details!'
                                });
                            }
                            if (result != null && result.length > 0) {
                                return res.status(200).json({
                                    token: token,
                                    statusCode: true,
                                    message: 'Auth Success!',
                                    data: result
                                });
                            }
                        });
                } else {
                    return res.status(200).json({
                        statusCode: false,
                        message: 'Auth Failed! Try again or click Forgot password to reset it.'
                    });
                }

            });
        })
        .catch(err => {
            console.log(err);
            res.status(500).json({
                error: err,
                statusCode: false
            });
        });
}

/**
 * Signup
 */
exports.signup = (req, res, next) => {
    User.find({ name: req.body.name })
        .exec()
        .then(user => {
            if (user.length >= 1) {
                return res.status(200).json({
                    statusCode: false,
                    message: 'User Already Exists!'
                });
            } else {
                bcrypt.hash(req.body.password, 10, (err, hash) => {
                    if (err) {
                        return res.status(200).json({
                            statusCode: false,
                            message: 'Error in Hashing Password'
                        });
                    } else {
                        const user = new User({
                            _id: new mongoose.Types.ObjectId(),
                            username: req.body.username,
                            password: hash,
                            name: req.body.name,
                            email: req.body.email,
                           // createdDate: new Date(),
                        });
                        user.save()
                            .then(result => {
                                return res.status(200).json({
                                    statusCode: true,
                                    message: 'User Created Successfully'
                                });
                            })
                            .catch(err => {
                                console.log(err);
                                res.status(500).json({
                                    error: err,
                                    statusCode: false
                                });
                            });
                    }
                })
            }
        })
        .catch(err => {
            console.log(err);
            res.status(500).json({
                error: err,
                statusCode: false
            });
        });
}

// /* Change Password */
// exports.changePass = (req, res, next) => {
//     var passwordDetails = req.body;

//     if (req.body.userId) {
//         if (passwordDetails.newPassword) {
//             User.find({ userId: req.body.userId })
//                 .exec()
//                 .then(user => {
//                     if (user.length < 1) {
//                         return res.status(200).json({
//                             statusCode: false,
//                             message: 'User Doesnot Exist!'
//                         });
//                     }
//                     bcrypt.compare(req.body.currentPassword, user[0].password, (err, result) => {
                        
//                         if (result) {
//                             if (req.body.newPassword === req.body.confirmPassword) {
//                                 bcrypt.hash(req.body.newPassword, 10, (err, hash) => {
//                                     if (err) {
//                                         return res.status(200).json({
//                                             statusCode: false,
//                                             message: 'Error in Password Change!'
//                                         });
//                                     } else {
//                                         User.findOneAndUpdate({ userId: req.body.userId },  { "$set" : { password : hash } }, { useFindAndModify: false })
//                                             .then(result => {
//                                                 if (!result) {
//                                                     return res.status(200).json({
//                                                         statusCode: false,
//                                                         message: 'Error in Password Change!'
//                                                     });
//                                                 } else {
//                                                     return res.status(200).json({
//                                                         statusCode: true,
//                                                         message: 'Password Updated Successfully!',
//                                                         data: JSON.parse(JSON.stringify(result)).userId
//                                                     });
//                                                 }
//                                             });
//                                     }
//                                 })
//                             } else {
//                                 return res.status(200).json({
//                                     statusCode: false,
//                                     message: 'New & Confirm Password Doesnot Match!'
//                                 });
//                             }

//                         } else {
//                             return res.status(200).json({
//                                 statusCode: false,
//                                 message: 'Wrong Current Password!'
//                             });
//                         }

//                     });
//                 })
//                 .catch(err => {
//                     console.log(err);
//                     res.status(500).json({
//                         error: err,
//                         statusCode: false
//                     });
//                 });
//         } else {
//             return res.status(401).send({
//                 statusCode: false,
//                 message: 'Please provide a new password'
//             });
//         }

//     } else {
//         return res.status(401).send({
//             statusCode: false,
//             message: 'User is not signed in'
//         });
//     }


// }