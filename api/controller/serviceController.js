const Service = require('../model/services');
const mongoose = require('mongoose');
var db = mongoose.connection;
var Schema = mongoose.Schema;
var serviceDetails = new Schema({});
var serviceObject = db.model('services', serviceDetails);
var serviceSort = { serviceId: -1 };

//Add new Service
exports.newService = (req, res, next) => {
    Service.find({ serviceName: req.body.serviceName })
        .exec()
        .then(service => {
            if (service.length >= 1) {
                return res.status(200).json({
                    statusCode: false,
                    message: 'Service already exists!'
                });
            } else {
                serviceObject.find({}, function (err, result) {
                    let sId;
                    if (result.length === 0) {
                        sId = 1
                    } else {
                        sId = result[0].serviceId + 1
                    }
                    if (err) {
                        res.status(500).json({
                            statusCode: false,
                            message: 'Something Went Wrong! Try Again',
                        });
                    } else {
                        const service = new Service(
                            {
                                serviceId:sId,
                                serviceName: req.body.serviceName,
                                serviceExcerpt: req.body.serviceExcerpt,
                                imageUrl: req.body.imageUrl,
                                createdDate: new Date()
                            }
                        );
                        service.save()
                            .then(result => {
                                return res.status(200).json({
                                    statusCode: true,
                                    message: 'Service added successfully!',
                                });
                            })
                            .catch(err => {
                                res.status(500).json({
                                    statusCode: false,
                                    error:err.message,
                                    message: 'Error adding service!',
                                });
                            });
                    }
                }).sort(serviceSort).limit(1)
            }
        })
        .catch(err => {
            console.log(err);
            res.status(500).json({
                error: err,
                statusCode: false,
                message: 'Something Went Wrong!',
            });
        });
}

//List all services
exports.listServices = (req, res, next) => {

    serviceObject.find({}, 'serviceName serviceExcerpt imageUrl'
        , function (err, result) {
            if (result != null && result.length > 0) {
                return res.status(200).json({
                    statusCode: true,
                    message: 'Services Retrived Successfully!',
                    data: result
                });
            } else {
                return res.status(200).json({
                    statusCode: false,
                    message: 'No Service Found!'
                });
            }

        }).catch(err => {
            res.status(500).json({
                error: err,
                statusCode: false,
                message: 'Something Went Wrong!',
            });
        });
}