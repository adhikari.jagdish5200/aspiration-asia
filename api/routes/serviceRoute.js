const express = require('express');
const router = express.Router();
const ServiceController = require('../controller/serviceController');

router.post('/newService', ServiceController.newService);
router.get('/listServices', ServiceController.listServices);

module.exports = router;