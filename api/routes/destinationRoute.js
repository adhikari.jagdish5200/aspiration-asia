const express = require('express');
const router = express.Router();
const DestinationController = require('../controller/destinationController');

router.post('/newDestination', DestinationController.newDestination);
router.post('/searchDestination', DestinationController.searchDestination);
module.exports = router;