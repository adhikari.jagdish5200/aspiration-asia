const express = require('express');
const router = express.Router();
const CarouselController = require('../controller/carouselController');

router.post('/newCarousel', CarouselController.newCarousel);
router.get('/listCarousel', CarouselController.listCarousel);

module.exports = router;