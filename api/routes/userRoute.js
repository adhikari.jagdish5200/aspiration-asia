const express = require('express');
const router = express.Router();
const UserController = require('../controller/userController');


router.post('/login', UserController.login);
router.post('/signup', UserController.signup);
//router.post('/changePassword', checkAuth, UserController.changePass);

module.exports = router;