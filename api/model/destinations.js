const mongoose = require('mongoose');

const destinationSchema = mongoose.Schema({
    destinationId: { type: Number, required: true },
    destinationName: { type: String, required: true },
    imageUrl: { type: String, required: true },
    createdDate: { type: Date, require: true },
});

module.exports = mongoose.model('Destination', destinationSchema);