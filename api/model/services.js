const mongoose = require('mongoose');

const servicesSchema = mongoose.Schema({
    serviceId: { type: Number, required: true },
    serviceName: { type: String, required: true },
    serviceExcerpt: { type: String, required: true },
    imageUrl: { type: String, required: true },
    createdDate: { type: Date, require: true },
});

module.exports = mongoose.model('Service', servicesSchema);