const mongoose = require('mongoose');

const carouselSchema = mongoose.Schema({
    imageUrl: { type: String, required: true },
    createdDate: { type: Date, require: true },
});

module.exports = mongoose.model('Carousel', carouselSchema);