const mongoose = require('mongoose');

const userSchema = mongoose.Schema({
    _id: mongoose.Schema.Types.ObjectId,
    username: { type: String, required: true },
    password: { type: String, require: true },
    name: { type: String },
    email: { type: String },
    createdDate: { type: Date, require: true },
});

module.exports = mongoose.model('User', userSchema);